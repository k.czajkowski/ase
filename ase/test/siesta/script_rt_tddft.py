import ase.calculators.siesta as siesta
from ase import Atoms
from ase.units import Ry, eV, Ha
import numpy as np
import matplotlib.pyplot as plt

Na8 = Atoms('Na8',
            positions=[[-1.90503810, 1.56107288, 0.00000000],
                        [1.90503810, 1.56107288, 0.00000000],
                        [1.90503810, -1.56107288, 0.00000000],
                        [-1.90503810, -1.56107288, 0.00000000],
                        [0.00000000, 0.00000000, 2.08495836],
                        [0.00000000, 0.00000000, -2.08495836],
                        [0.00000000, 3.22798122, 2.08495836],
                        [0.00000000, 3.22798122, -2.08495836]],
            cell=[20, 20, 20])

siesta = siesta.Siesta(
    mesh_cutoff= 250 * Ry,
    basis_set='DZP',
    pseudo_qualifier='',
    energy_shift=(100 * 10 ** -3) * eV,
    fdf_arguments={
        'COOP.Write': True,
        'WriteDenchar': True,
        'PAO.BasisType': 'split',
        'DM.Tolerance': 1e-4,
        'DM.MixingWeight': 0.01,
        'MaxSCFIterations': 1500, 
        'DM.NumberPulay': 4,
        'XML.Write': True})
kick_to_au=51.422
siesta.parameters.update({'tddft':{'time_step':7.4e-3,'Nsteps':2800,'external_field':(1e-5*kick_to_au,0.0,0.0)}})
siesta.calculate(atoms=Na8,properties=['tddft1'])
siesta.calculate(atoms=Na8,properties=['tddft2'])
omegas=np.linspace(1,6,101)
pol=siesta.polarizability_spectrum(omegas=omegas)
np.savetxt('pol.txt',np.array([pol,omegas]))
plt.plot(omegas,pol)
plt.xlabel('Energy (eV)')
plt.ylabel('Imag(P$_{xx}$) (a.u.)')
plt.savefig('pol.png',dpi=400,bbox_inches='tight')
